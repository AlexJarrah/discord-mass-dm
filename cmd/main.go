package main

import (
	"log"

	"gitlab.com/AlexJarrah/discord-mass-dm/internal/discord"
	"gitlab.com/AlexJarrah/discord-mass-dm/internal/files"
)

func main() {
	// Ensure necessary files exist and are properly initialized
	if err := files.Initialize(); err != nil {
		log.Fatalf("failed to initialize files: %v", err)
	}

	// Parse configuration file to validate configuration structure
	if err := files.ParseConfig(); err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// Ensure all necessary configuration fields are properly populated
	if err := files.ValidateConfig(); err != nil {
		log.Fatalf("failed to validate configuration: %v", err)
	}

	// Initialize Discord connection & start automation
	if err := discord.Start(); err != nil {
		log.Fatalf("failed to establish Discord connection: %v", err)
	}
}
