package discord

import (
	"fmt"
	"log"
	"time"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/AlexJarrah/discord-mass-dm/internal"
)

// Start establishes a Discord connection via the provided token and starts task automation
func Start() error {
	// Establish a Discord session
	dg, err := discordgo.New(internal.Config.DiscordToken)
	if err != nil {
		return fmt.Errorf("failed to create session: %w", err)
	}

	// Request all intents for the session
	dg.Identify.Intents = discordgo.IntentsAll

	// Set the session user agent to improve anti-bot evasion
	dg.UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:133.0) Gecko/20100101 Firefox/133.0"

	// Call ready handler once an established connection has been made
	dg.AddHandler(ready)

	// Open a websocket connection
	if err := dg.Open(); err != nil {
		return err
	}
	defer dg.Close()

	// Await explicit termination signal to allow time for tasks to complete
	select {}
}

// ready executes main program logic once a successful Discord connection has been established
func ready(s *discordgo.Session, m *discordgo.Ready) {
	// Retry on error
	for {
		if err := execute(s, m); err != nil {
			log.Printf("execution error: %v", err)
			time.Sleep(2 * time.Second)
			continue
		}

		log.Println("All operations complete successfully. Press CTRL+C to exit.")
		return
	}
}
