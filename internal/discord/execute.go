package discord

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/AlexJarrah/discord-mass-dm/internal"
)

// clear clears the terminal screen
func clear() {
	fmt.Print("\033[H\033[2J")
}

func execute(s *discordgo.Session, m *discordgo.Ready) error {
	// Prompt the user to select a target guild
	guild, err := promptGuild(m.Guilds)
	if err != nil {
		// Retry selection on error
		log.Println("Error:", err)
		time.Sleep(time.Second)
		return execute(s, m)
	}

	// Clear terminal screen
	clear()

	// Await countdown to provide opportunity to cancel operation
	for countdown := 5; countdown >= 0; countdown-- {
		if countdown == 0 {
			clear()
			fmt.Printf("\rStarting execution in %s.\n", guild.Name)
		} else {
			fmt.Printf("\rStarting execution in %s (%d)... (Press CTRL+C to cancel)", guild.Name, countdown)
		}
		time.Sleep(time.Second)
	}

	// Send message to all guild members
	messageMembers(s, m, guild.Members)
	return nil
}

// Below function is no longer supported for user accounts. Users must only
// retrieve guild members via gateway commands & subscriptions.
//
// func getGuildMembers(s *discordgo.Session, guildID string) ([]*discordgo.Member, error) {
// 	done := make(chan struct{}, 1)
// 	var chunk uint8
// 	rmfn := s.AddHandler(func(s *discordgo.Session, c *discordgo.GuildMembersChunk) {
// 		chunk++
// 		fmt.Printf("\rRetrieving server members... (%d)", chunk)
// 		if c.ChunkIndex == c.ChunkCount-1 {
// 			fmt.Printf("\rFetched all server members\n")
// 			done <- struct{}{}
// 		}
// 	})
// 	defer rmfn()
// 	err := s.RequestGuildMembers(guildID, "", 0, time.Now().String(), false)
// 	if err != nil {
// 		return nil, fmt.Errorf("failed to request guild members: %w", err)
// 	}
// 	<-done
//
// 	guild, err := s.State.Guild(guildID)
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return guild.Members, nil
// }

// getGuildMembers requests all members of a specified guild in chunks and returns the result
func getGuildMembersa(s *discordgo.Session, guildID string) ([]*discordgo.Member, error) {
	var members, batch []*discordgo.Member

	// Retrieve all members in batches for the user specified guild
	for len(batch) == 1000 || len(members) == 0 {
		// Use the last user of the previous batch to handle pagination
		var lastID string = "0"
		if len(batch) != 0 {
			lastID = batch[len(batch)-1].User.ID
		}

		// Request the next 1000 guild members
		fmt.Println(guildID)
		batch, err := s.GuildMembers(guildID, lastID, 1000)
		if err != nil {
			return nil, fmt.Errorf("failed to get valid response: %w", err)
		} else if len(batch) == 0 {
			return nil, errors.New("no members in server")
		}

		// Add the batch results to the members slice
		members = append(members, batch...)
	}

	return members, nil
}

// promptGuild prompts the user to select a target guild
func promptGuild(guilds []*discordgo.Guild) (*discordgo.Guild, error) {
	// Clear terminal screen
	clear()

	// Create list format
	length := len(fmt.Sprint(len(guilds)))
	format := fmt.Sprintf("%%%dd: %%s\n", length)

	// List all user guilds by name
	for i, g := range guilds {
		fmt.Printf(format, i+1, g.Name)
	}

	// Prompt user to select a guild to automate tasks on
	fmt.Print("Select target server: ")

	// Store user input
	var input string
	fmt.Scanln(&input)

	// Parse user input as uint
	index, err := strconv.ParseUint(input, 0, 8)
	if err != nil {
		return nil, fmt.Errorf("invalid input: %w", err)
	} else if uint8(index) > uint8(len(guilds)) {
		return nil, fmt.Errorf("invalid input: %d exceeds the number of valid servers available (%d)", uint8(index), length)
	}

	// Return selected guild
	selectedGuild := guilds[index-1]
	return selectedGuild, nil
}

// messageMembers messages all guild members based on configured role rules
func messageMembers(s *discordgo.Session, r *discordgo.Ready, members []*discordgo.Member) {
	// Clear terminal screen
	clear()

	// Helper function to check role-based inclusion/exclusion
	isValidMember := func(member *discordgo.Member) bool {
		// Skip self and bots
		if member.User.ID == r.User.ID || member.User.Bot {
			return false
		}

		// Track if a wildcard exclusion is present
		hasWildcardExclude := false

		// Check exclusion roles
		for _, excludeRole := range internal.Config.Roles.Exclude {
			for _, memberRole := range member.Roles {
				if memberRole == excludeRole {
					return false // Explicitly excluded
				}
				if excludeRole == "*" {
					hasWildcardExclude = true
				}
			}
		}

		// Check inclusion roles
		for _, includeRole := range internal.Config.Roles.Include {
			for _, memberRole := range member.Roles {
				if memberRole == includeRole {
					return true
				} else if includeRole == "*" && !hasWildcardExclude {
					return true
				}
			}
		}

		return false
	}

	// Prepare a personalization replacer
	replacer := strings.NewReplacer(
		"$USERNAME", "",
		"$USER_ID", "",
		"$MENTION", "",
		"$NICK", "",
		"$AVATAR_URL", "",
	)

	// Iterate through members and send messages
	for i, member := range members {
		// Skip members that don't meet role criteria
		if !isValidMember(member) {
			continue
		}

		// Select message from pool cyclically
		messageNo := i % len(internal.Config.MessagePool)
		message := internal.Config.MessagePool[messageNo]

		// Personalize the message with member-specific details via variables
		message = replacer.Replace(message)
		message = strings.ReplaceAll(message, "$USERNAME", member.User.Username)
		message = strings.ReplaceAll(message, "$USER_ID", member.User.ID)
		message = strings.ReplaceAll(message, "$MENTION", member.User.Mention())
		message = strings.ReplaceAll(message, "$NICK", member.Nick)
		message = strings.ReplaceAll(message, "$AVATAR_URL", member.AvatarURL("512"))

		// Log the message attempt
		log.Printf("Sending message #%d to %s (%s)", messageNo+1, member.User.Username, member.User.ID)

		// Attempt to create a direct message channel
		userChannel, err := s.UserChannelCreate(member.User.ID)
		if err != nil {
			log.Printf("Failed to create user channel for %s: %v", member.User.Username, err)
			continue
		}

		// Send the personalized message
		_, err = s.ChannelMessageSend(userChannel.ID, message)
		if err != nil {
			log.Printf("Failed to send message to %s: %v", member.User.Username, err)
			continue
		}
	}
}
