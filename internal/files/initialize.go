package files

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/AlexJarrah/discord-mass-dm/internal"
)

// Initialize ensures all necessary files exist and are properly structured
func Initialize() error {
	// Return if the config file exists
	if _, err := os.Stat(configFilePath); err == nil {
		return nil
	}

	// Ensure the data directory exists
	if err := os.MkdirAll("data", 0755); err != nil {
		return fmt.Errorf("failed to create data directory: %w", err)
	}

	// Create the config file
	file, err := os.Create(configFilePath)
	if err != nil {
		return fmt.Errorf("failed to create configuration file: %w", err)
	}
	defer file.Close()

	// Define default configuration
	defaultConfig := internal.Configuration{
		DiscordToken: "",
		MessagePool:  []string{},
		Roles: internal.Roles{
			Include: []string{"*"},
			Exclude: []string{""},
		},
	}

	// Encode JSON to config file
	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	if err := encoder.Encode(defaultConfig); err != nil {
		return fmt.Errorf("failed to write default configuration to file: %w", err)
	}

	return nil
}
