package files

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"gitlab.com/AlexJarrah/discord-mass-dm/internal"
)

// ParseConfig ensures validity of configuration structure
func ParseConfig() error {
	// Read configuration from config file
	file, err := os.ReadFile(configFilePath)
	if err != nil {
		return fmt.Errorf("failed to read configuration file: %w", err)
	}

	// Ensure file is not empty
	if len(file) == 0 {
		return errors.New("configuration file is empty")
	}

	// Unmarshal JSON contents of the config file into the global config variable,
	// returning an error if the structure is not valid
	if err = json.Unmarshal(file, &internal.Config); err != nil {
		return fmt.Errorf("failed to parse JSON configuration: %w", err)
	}

	return nil
}

// ValidateConfig validates the configuration contents, returning an error
// if a necessary field is not properly populated
func ValidateConfig() error {
	if internal.Config.DiscordToken == "" {
		return errors.New("no user token provided")
	} else if len(internal.Config.MessagePool) == 0 {
		return errors.New("no messages provided in pool")
	}

	return nil
}
